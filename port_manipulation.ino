// Copyright (c) 2019 Abanoub Sameh

// **this is the same as hacky_hack_hack.ino**
// you can read it instead

#define LED 13  // led to toggle

void setup() {
    // use port manipulation
  DDRB = B00100000;

  noInterrupts();
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;

  OCR1A = 31250;
  // set up the timer
  TCCR1B = (TCCR1B & B11110111) | B00001000;    // CTC mode
  TCCR1B = (TCCR1B & B11111000) | B00000100;    // setting the prescaler to 256
  TIMSK1 = (TIMSK1 & B11111001) | B00000110;    // enable compare interrupts
  interrupts();
}

// interrupt handler
ISR(TIMER1_COMPA_vect) {
  PORTB = (PINB ^ B00100000) & B00100000;
}

// here loop is not used
void loop() {

}
