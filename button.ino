// Copyright (c) 2019 Abanoub Sameh

#define LED 13      // LED is defined as 13
#define BUTTON 2    /// BUTTON is defined as 2

void setup() {  // put your code here to run once
    pinMode(BUTTON, INPUT);     // declare BUTTON as INPUT. This approach needs a resistor to connect the pin to VCC or GND
                                // use a 10K ohm or 20K ohm resistor as a pullup or pulldown resistor
    pinMode(LED, OUTPUT);       // declare LED as OUTPUT
}

void loop() {   // put your code here to run repeatedly
    if (digitalRead(BUTTON)) {      // here we read the button state
        // ^ this function returns true if the button is HIGH otherwise it returns false
        // if true this code will run
        digitalWrite(LED, HIGH);
    } else {
        // if the button is false then this will run
        digitalWrite(LED, LOW);
    }
}
