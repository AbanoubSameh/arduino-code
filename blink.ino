// Copyright (c) 2019 Abanoub Sameh

#define LED 13  // everytime the compiler sees LED it will replace it with 13
    //  ^ here LED is written all in caps because it is a constant
    // this approach permits us to change the pin once here and it will be changed in all the code

void setup() {  // put your code here to run once
    pinMode(LED, OUTPUT);   // here we declare that the LED pin will be used as output
}

void loop() {   // put your code here to run repeatedly
    digitalWrite(LED, HIGH);    // here we set the LED pin HIGH
    delay(100);                 // we wait for 100 ms
    digitalWrite(LED, LOW);     // then we set it LOW
    delay(100);                 // and finally we wait for 100ms again
    // NOTE!! ^ never ever forget the semicolon
    // also never foget to close the bracket down here
}
