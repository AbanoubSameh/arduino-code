// Copyright (c) 2019 Abanoub Sameh

#define LEDS B00110000  // we define the leds that we will toggle

void setup() {
  DDRB = LEDS;      // use this to write this value to portb

  noInterrupts();   // we don't want interrupt now
  // here we are setting up the timer
  // we need to set all these registers to zero
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;

  OCR1A = 31250;    // 16MHz / 256 / 2Hz

  TCCR1B = (TCCR1B & B11110111) |    B00001000;    // CTC mode
// leave those alone  ^^^^ ^^^   ,set this^

  TCCR1B = (TCCR1B & B11111000) | B00000100;    // setting the prescaler to 256
// leave those alone  ^^^^^   ,set this ^

  TIMSK1 = (TIMSK1 & B11111001) | B00000110;    // enable compare interrupts
// leave those alone  ^^^^^  ^,set these^^

  interrupts();     // now we can handle interrupts
}

// this is the timer interrupt handler
ISR(TIMER1_COMPA_vect) {
    // use port manipulation to change the leds state with xor
  PORTB = (PINB ^ B11111111) & LEDS;
    // and use and here      ^ to leave the rest or the port alone
}

// in this sketch loop is not used
void loop() {

}
