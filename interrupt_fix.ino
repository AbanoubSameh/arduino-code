// Copyright (c) 2019 Abanoub Sameh

// I think that this file has something weird going on in it

int button = 2;
int value = 0;
int last = 0;
int GND = 4;
bool buttonReleased = false;

void increment() {
  int time = millis();
  if (time - last > 150) {
    value++;
    Serial.println(value);
  }
  last = time;
  digitalWrite(13, HIGH);
  delay(2000);
  digitalWrite(GND, LOW);
}

void setTrue() {
  int time = millis();
  if (!buttonReleased) {
    buttonReleased = true;
    Serial.println("Button Released");
    attachInterrupt(digitalPinToInterrupt(button), increment, RISING);
  }
  last = time;
}

void setup() {
  Serial.begin(9600);
  pinMode(GND, OUTPUT);
  digitalWrite(GND, HIGH);
  pinMode(button, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(button), setTrue, FALLING);
  Serial.println(value);

  pinMode(13, OUTPUT);
}

void loop() {

}
