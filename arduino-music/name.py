import os
import sys

def save_nums():
    num_file = open("/home/abanoub/data/num_file", "w")
    num_file.write(str(anum))
    num_file.write("\n")
    num_file.write(str(cnum))
    num_file.write("\n")
    num_file.write(str(enum))
    num_file.write("\n")
    num_file.write(str(fnum))
    num_file.write("\n")
    num_file.write(str(gnum))
    num_file.write("\n")
    num_file.write(str(hnum))
    num_file.write("\n")
    num_file.write(str(inum))
    num_file.write("\n")
    num_file.write(str(jnum))
    num_file.write("\n")
    num_file.write(str(nnum))
    num_file.write("\n")
    num_file.write(str(anum))
    num_file.write("\n")
    num_file.write(str(onum))
    num_file.write("\n")
    num_file.write(str(pnum))
    num_file.write("\n")
    num_file.write(str(snum))
    num_file.close()
    sys.exit(0)

def get_num(letter):
    if letter == "a":
        global anum
        anum += 1
        return anum
    elif letter == "c":
        global cnum
        cnum += 1
        return cnum
    elif letter == "e":
        global enum
        enum += 1
        return enum
    elif letter == "f":
        global fnum
        fnum += 1
        return fnum
    elif letter == "g":
        global gnum
        gnum += 1
        return gnum
    elif letter == "h":
        global hnum
        hnum += 1
        return hnum
    elif letter == "i":
        global inum
        inum += 1
        return inum
    elif letter == "j":
        global jnum
        jnum += 1
        return jnum
    elif letter == "n":
        global nnum
        nnum += 1
        return nnum
    elif letter == "o":
        global onum
        onum += 1
        return onum
    elif letter == "p":
        global pnum
        pnum += 1
        return pnum
    elif letter == "s":
        global snum
        snum += 1
        return snum
    elif letter == "x":
        return -3
    elif letter == "q":
        return -2
    else:
        return -1

os.chdir("/home/abanoub/data/arduino_music")

test = os.listdir()

num_file = open("/home/abanoub/data/num_file", "r+")

lines = num_file.read().split()

anum = int(lines[0]) - 1
cnum = int(lines[1]) - 1
enum = int(lines[2]) - 1
fnum = int(lines[3]) - 1
gnum = int(lines[4]) - 1
hnum = int(lines[5]) - 1
inum = int(lines[6]) - 1
jnum = int(lines[7]) - 1
nnum = int(lines[8]) - 1
onum = int(lines[9]) - 1
pnum = int(lines[10]) - 1
snum = int(lines[11]) - 1

print(anum)
print(cnum)
print(enum)
print(fnum)
print(gnum)
print(hnum)
print(inum)
print(jnum)
print(nnum)
print(onum)
print(pnum)
print(snum)

for i in test:
    letter = input("choose a letter for {}\n".format(i))
    num = get_num(letter)

    if num == -1:
        print("wrong letter, ignoring", i)
    elif num == -2:
        print("quit signal received exiting")
        save_nums()
    elif num == -3:
        print("ignore signal received")
        continue
    else:
        name = letter + str(format(num, '03d'))
        print(name)
        os.system("mv '{}' {}".format(i, name))

save_nums()
