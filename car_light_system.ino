// Copyright (c) 2019 Abanoub Sameh

//#include <EEPROM.h>
//#include <sstrings2.h>

#define RATIO 2.55
#define SPARK 2

byte led1 = 5;
byte led2 = 6;
byte led3 = 9;
byte led4 = 11;
byte motor = 3;

byte v1 = 255;
byte v2 = 255;
byte v3 = 255;
byte v4 = 255;

byte state1 = 0;
byte state2 = 0;
byte state3 = 0;
byte state4 = 0;

char temp[1000];

char *tempPointer1;
char *tempPointer2;
char *tempPointer3;
char *tempPointer4;
char *tempPointerS;

char *lastChar(char *string, char letter) {

    for (int i = strlen(string) - 1; i >= 0; i--) {
        if (string[i] == letter) {
            return string + i;
        }
    }
    return NULL;

}

void setup() {
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  pinMode(motor, OUTPUT);

  Serial.begin(9600);
/*
  if (EEPROM.read(led1) == HIGH) {
      digitalWrite(led1, HIGH);
  }
  if (EEPROM.read(led2) == HIGH) {
      digitalWrite(led2, HIGH);
  }
  if (EEPROM.read(led3) == HIGH) {
      digitalWrite(led3, HIGH);
  }
  if (EEPROM.read(led4) == HIGH) {
      digitalWrite(led4, HIGH);
  }
*/
}

void loop() {

  if (Serial.available()) {
    Serial.readString().toCharArray(temp, 50); // needs to change to 1000

    Serial.println(temp);

    tempPointer1 = lastChar(temp, 'A');
    tempPointer2 = lastChar(temp, 'B');
    tempPointer3 = lastChar(temp, 'C');
    tempPointer4 = lastChar(temp, 'D');
    tempPointerS = lastChar(temp, 'S');

    if (tempPointer1) {
      if (tempPointer1[1] == '1') {
        analogWrite(led1, v1);
        state1 = 1;
        //  EEPROM.write(led1, 1);
      } else if (tempPointer1[1] == '0') {
        analogWrite(led1, 0);
        state1 = 0;
        //  EEPROM.write(led1, 0);
      }
    }

    if (tempPointer2) {
      if (tempPointer2[1] == '1') {
        analogWrite(led2, v2);
        state2 = 1;
        //  EEPROM.write(led2, 1);
      } else if (tempPointer2[1] == '0') {
        analogWrite(led2, 0);
        state2 = 0;
        //  EEPROM.write(led2, 0);
      }
    }

    if (tempPointer3) {
      if (tempPointer3[1] == '1') {
        analogWrite(led3, v3);
        state3 = 1;
        //  EEPROM.write(led3, 1);
      } else if (tempPointer3[1] == '0') {
        analogWrite(led3, 0);
        state3 = 0;
        //  EEPROM.write(led3, 0);
      }
    }

    if (tempPointer4) {
      if (tempPointer4[1] == '1') {
        analogWrite(led4, v4);
        state4 = 1;
        //  EEPROM.write(led4, 1);
      } else if (tempPointer4[1] == '0') {
        analogWrite(led4, 0);
        state4 = 0;
        //  EEPROM.write(led4, 0);
      }
    }

    if (tempPointerS && strstr(tempPointerS, "tart")) {
      Serial.write("starting motor\n");
      digitalWrite(motor, 1);
      delay(SPARK * 1000);
      digitalWrite(motor, 0);
    }

    tempPointer1 = lastChar(temp, 'a');
    tempPointer2 = lastChar(temp, 'b');
    tempPointer3 = lastChar(temp, 'c');
    tempPointer4 = lastChar(temp, 'd');

    if (tempPointer1 && tempPointer1[1] != 'r') {
      char charValue[4];
      memcpy(charValue, tempPointer1 + 1, 3);
      charValue[3] = 0;
      int value = atoi(charValue);
      v1 = value * RATIO;
      Serial.println(v1);
      if (state1 == 1) {
        analogWrite(led1, v1);
      }
    }

    if (tempPointer2) {
      char charValue[4];
      memcpy(charValue, tempPointer2 + 1, 3);
      charValue[3] = 0;
      int value = atoi(charValue);
      v2 = value * RATIO;
      if (state2 == 1) {
        analogWrite(led2, v2);
      }
    }

    if (tempPointer3) {
      char charValue[4];
      memcpy(charValue, tempPointer3 + 1, 3);
      charValue[3] = 0;
      int value = atoi(charValue);
      v3 = value * RATIO;
      if (state3 == 1) {
        analogWrite(led3, v3);
      }
    }

    if (tempPointer4) {
      char charValue[4];
      memcpy(charValue, tempPointer4 + 1, 3);
      charValue[3] = 0;
      int value = atoi(charValue);
      v4 = value * RATIO;
      if (state4 == 1) {
        analogWrite(led4, v4);
      }
    }

/*
    if (strstr(temp, "T1")) {
      digitalWrite(led1, HIGH);
      digitalWrite(led2, HIGH);
      digitalWrite(led3, HIGH);
      digitalWrite(led4, HIGH);
    }

    if (strstr(temp, "T0")) {
      digitalWrite(led1, LOW);
      digitalWrite(led2, LOW);
      digitalWrite(led3, LOW);
      digitalWrite(led4, LOW);
    }
*/

    if (strstr(temp, "INIT")) {
      if (state1 == HIGH) {
        Serial.write(1);
      } else {
        Serial.write(0);
      }

      if (state2 == HIGH) {
        Serial.write(1);
      } else {
        Serial.write(0);
      }

      if (state3 == HIGH) {
        Serial.write(1);
      } else {
        Serial.write(0);
      }

      if (state4 == HIGH) {
        Serial.write(1);
      } else {
        Serial.write(0);
      }

      char buffer[5];

      sprintf(buffer, "%03d", v1);
      Serial.println(buffer);

      sprintf(buffer, "%03d", v2);
      Serial.println(buffer);

      sprintf(buffer, "%03d", v3);
      Serial.println(buffer);

      sprintf(buffer, "%03d", v4);
      Serial.println(buffer);

    }

  }

}
