// Copyright (c) 2019 Abanoub Sameh

const int UP1 = 12, DOWN1 = 11, UP2 = 10, DOWN2 = 9, RESET = 8;

int counter1 = 0;
int u1State = 0;
int lastU1State = 0;

int counter2 = 0;
int u2State = 0;
int lastU2State = 0;

int var1 = 0;
int d1State = 0;
int lastD1State = 0;

int var2 = 0;
int d2State = 0;
int lastD2State = 0;

int resetState = 0;
int lastResetState = 0;

void setup() {

  Serial.begin(9600);

  pinMode(UP1, INPUT_PULLUP);
  pinMode(UP2, INPUT_PULLUP);
  pinMode(DOWN1, INPUT_PULLUP);
  pinMode(DOWN2, INPUT_PULLUP);
  pinMode(RESET, INPUT_PULLUP);

}

void loop() {

  u1State = digitalRead(UP1);
  u2State = digitalRead(UP2);
  d1State = digitalRead(DOWN1);
  d2State = digitalRead(DOWN2);
  resetState = digitalRead(RESET);

  if (u1State != lastU1State && u1State == LOW) {
    var1++;
    Serial.print("people waiting at A: ");
    Serial.println(var1);
  }

  if (u2State != lastU2State && u2State == LOW) {
    var2++;
    Serial.print("people waiting at B: ");
    Serial.println(var2);
  }

  if (d1State != lastD1State && d1State == LOW) {
    if (var1 != 0) {
      counter1++;
      Serial.print("A: ");
      Serial.println(counter1);
      var1--;
    } else {
      Serial.println("A is empty");
    }
  }

  if (d2State != lastD2State && d2State == LOW) {
    if (var2 != 0) {
      counter2++;
      Serial.print("B: ");
      Serial.println(counter2);
      var2--;
    } else {
      Serial.println("B is empty");
    }
  }

  if (resetState != lastResetState && resetState == LOW) {
    counter1 = 0;
    u1State = 0;
    lastU1State = 0;

    counter2 = 0;
    u2State = 0;
    lastU2State = 0;

    var1 = 0;
    d1State = 0;
    lastD1State = 0;

    var2 = 0;
    d2State = 0;
    lastD2State = 0;
  }

  lastU1State = u1State;
  lastU2State = u2State;
  lastD1State = d1State;
  lastD2State = d2State;
  lastResetState = resetState;

  delay(25);

}
