// Copyright (c) 2019 Abanoub Sameh

// my music player source, this is incomplete, the complete version has been lost

#include "SD.h"
#include "TMRpcm.h"
#include "SPI.h"

#define SD_ChipSelectPin 10

int volumeValue = 5;

TMRpcm tmrpcm;

void increase() {

  volumeValue++;
  tmrpcm.setVolume(volumeValue);
  Serial.println("volume equal: " + volumeValue);

}

void decrease() {

  volumeValue--;
  tmrpcm.setVolume(volumeValue);
  Serial.println("volume equal: " + volumeValue);

}

void setup() {
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(2), increase, FALLING);
  attachInterrupt(digitalPinToInterrupt(3), decrease, FALLING);

  tmrpcm.speakerPin=9;
  Serial.begin(9600);

  if(!SD.begin(SD_ChipSelectPin)) {
    Serial.println("SD fail");
    return;
  }

  Serial.println("playing music");
  tmrpcm.setVolume(volumeValue);
  //char test[2];
  //itoa(1, test, 2);
  tmrpcm.play("dixie.wav");

}

void loop() {
  // put your main code here, to run repeatedly:

}
