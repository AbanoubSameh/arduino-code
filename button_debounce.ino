// Copyright (c) 2019 Abanoub Sameh

#define LED     13      // the led and the button pins
#define BUTTON  2

// we use the word volatile because this is shared between a function and an interrupt handler
volatile bool isClicked = false;
bool state = true;

// interrupt handler
void do_something() {
  isClicked = true;     // sets this variable to true to communicate with loop
}

void setup() {
  // setting up the pins
  pinMode(LED, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);

  // this function is used to tie this button to this ↓ function
  attachInterrupt(digitalPinToInterrupt(BUTTON), do_something, FALLING);
  // be sure to use ^ this function to get the button           ^ and this is the
  //                                                        action that will trigger the interrupt
}

void loop() {
  if (isClicked) {      // if the button had been clicked
    delay(150);         // delay to debounce the button
    state = !state;     // we change the state
    isClicked = false;  // make sure to return isClicked to its normal state
  }
  if (state) {
    digitalWrite(LED, HIGH);
  } else {
    digitalWrite(LED, LOW);
  }
}
