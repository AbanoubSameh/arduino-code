// Copyright (c) 2019 Abanoub Sameh

// **this file is a refrence only and does not work as intended**

#define FLED 6      // first led pin
#define DE   200    // delay time

byte32 NLED = 8 ;

void setup() {
    // all these are going to be used as output
  for (int i = 0; i < NLED; i++) {
    pinMode(FLED + i, OUTPUT);
  }

    // we define all those at input pullup becasue we are going to use them
    // as binary input
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);

  Serial.begin(9600);
}

void loop() {
    // first part all the leds are flashed
  for (int i = 0; i < NLED; i++) {
    digitalWrite(FLED + i, HIGH);
    delay(DE);
    digitalWrite(FLED + i, LOW);
  }
    // second part, the same but in the reverse order
  for (int i = NLED; i > 0; i--) {
    digitalWrite(FLED + i, HIGH);
    delay(DE);
    digitalWrite(FLED + i, LOW);
  }
    // then flashed inward
  for (int i = 0; i < NLED; i++) {
    digitalWrite(FLED + i, HIGH);
    digitalWrite(FLED + NLED - i - 1, HIGH);
    delay(DE);
    digitalWrite(FLED + i, LOW);
    digitalWrite(FLED + NLED - i - 1, LOW);
  }
    // then flashed outward
  for (int i = NLED; i > 0; i--) {
    digitalWrite(FLED + i, HIGH);
    digitalWrite(FLED + NLED - i - 1, HIGH);
    delay(DE);
    digitalWrite(FLED + i, LOW);
    digitalWrite(FLED + NLED - i - 1, LOW);
  }
    // then we see if our input has changed so we change
    the value of the number of leds accordingly
  NLED = !digitalRead(3) * 4 + !digitalRead(4) * 2 + !digitalRead(5);
  Serial.println(!digitalRead(3) * 4 + !digitalRead(4) * 2 + !digitalRead(5));
}
