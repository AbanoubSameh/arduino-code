// Copyright (c) 2019 Abanoub Sameh

void setup() {
  DDRB = 1;     // this means to set portd to 1

  // setting up the timer SEE THE ARDUINO DOCUMENTATION
  noInterrupts();   // this disables interrupts

  TCCR1A = 0;   // this sets the timer1 a register to 0
  TCCR1B = 0;   // this sets the timer1 b register to 0
  TCNT1  = 0;   // this sets the timer1 TCNT register to 0

  OCR1A = 31250;    // this is the value that triggers
                    // the timer interrupt when reached

  TCCR1B |= (1 << WGM12);   // here we are setting the timer mode
  TCCR1B |= (1 << CS12);    // here we are setting the prescaler
  TIMSK1 |= (1 << OCIE1A);  // here we enable timer compare interrupt

  interrupts();             // enable all interrupts
}

// loop not used
void loop() {

}
