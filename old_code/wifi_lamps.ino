// I don't think this code was not written by me and I don't know if it works

/*
  WiFiHTTPSServer - Simple SSL server example

  This sketch demonstrates how to set up a simple HTTP-like server usingcl
  HTTPS encryption. This is NOT the recommended way of writing a web server!
  Please see the ESP8266WebServer and ESP8266WebServerSecure for a much easier
  and more robust server for your own applications.

  The server will set a GPIO pin depending on the request
    https://server_ip/gpio/0 will set the GPIO2 low,
    https://server_ip/gpio/1 will set the GPIO2 high
  server_ip is the IP address of the ESP8266 module, will be
  printed to Serial when the module is connected.

  IMPORTANT NOTES ABOUT SSL CERTIFICATES

  1. USE/GENERATE YOUR OWN CERTIFICATES
    While a sample, self-signed certificate is included in this example,
    it is ABSOLUTELY VITAL that you use your own SSL certificate in any
    real-world deployment.  Anyone with the certificate and key may be
    able to decrypt your traffic, so your own keys should be kept in a
    safe manner, not accessible on any public network.

  2. HOW TO GENERATE YOUR OWN CERTIFICATE/KEY PAIR
    A sample script, "make-self-signed-cert.sh" is provided in the
    ESP8266WiFi/examples/WiFiHTTPSServer directory.  This script can be
    modified (replace "your-name-here" with your Organization name).  Note
    that this will be a *self-signed certificate* and will *NOT* be accepted
    by default by most modern browsers.  They'll display something like,
    "This certificate is from an untrusted source," or "Your connection is
    not secure," or "Your connection is not private," and the user will
    have to manully allow the browser to continue by using the
    "Advanced/Add Exception" (FireFox) or "Advanced/Proceed" (Chrome) link.

    You may also, of course, use a commercial, trusted SSL provider to
    generate your certificate.  When requesting the certificate, you'll
    need to specify that it use SHA256 and 1024 or 512 bits in order to
    function with the axTLS implementation in the ESP8266.

  Adapted by Earle F. Philhower, III, from the WiFiWebServer.ino example.
  This example is released into the public domain.
*/

#include <ESP8266WiFi.h>

#ifndef STASSID
#define STASSID "wifi name goes here"
#define STAPSK  "password goes here"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);

void setup() {
  Serial.begin(115200);

  // prepare GPIO2
  pinMode(2, OUTPUT);
  digitalWrite(2, 0);

  pinMode(3, OUTPUT);
  digitalWrite(3, 0);

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());
}

void loop() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  // Wait until the client sends some data
  Serial.println("new client");
  unsigned long timeout = millis() + 3000;
  while (!client.available() && millis() < timeout) {
    delay(1);
  }
  if (millis() > timeout) {
    Serial.println("timeout");
    client.flush();
    client.stop();
    return;
  }

  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();

  // Match the request
  if (req.indexOf("/gpio/0") != -1) {
    digitalWrite(2, 0);
    client.println("done");
  } else if (req.indexOf("/gpio/1") != -1) {
    digitalWrite(2, 1);
    client.println("done");
  } else if (req.indexOf("/gpio/2") != -1) {
    digitalWrite(3, 0);
    client.println("done");
  } else if (req.indexOf("/gpio/3") != -1) {
    digitalWrite(3, 1);
    client.println("done");
  } else {
    Serial.println("invalid request");
    client.print("HTTP/1.1 404 Not Found\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html><body>Not found</body></html>");
    return;
  }

  client.flush();

  delay(1);
  Serial.println("Client disconnected");

  // The client will actually be disconnected
  // when the function returns and 'client' object is detroyed
}
