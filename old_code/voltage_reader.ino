// old code, don't know if it works

#define NUM_OF_SAMPLES 10
#define DIVIDER_RATIO 4.3182
// 4.3182 for 21.591v R1 = 73 R2 = 22
// 21.4 for 107v R1 = 102 R2 = 5

int sum = 0;
unsigned char sample_count = 0;
double voltage = 0.0;

void setup() {
  Serial.begin(9600);
}

void loop() {

  while (sample_count < NUM_OF_SAMPLES) {
    sum += analogRead(A5);
    sample_count++;
    delay(10);
  }
  voltage = ((double) sum / (double) NUM_OF_SAMPLES * DIVIDER_RATIO) / 204.8;
  Serial.print(voltage);
  Serial.println(" V");
  sample_count = 0;
  sum = 0;
}
