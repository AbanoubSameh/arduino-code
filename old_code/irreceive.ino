// old code, don't know if it works

#include <IRremote.h>
#include <IRremoteInt.h>

int value = 0;

IRrecv receive(12);
decode_results results;

void setup() {

  pinMode(13, OUTPUT);

  Serial.begin(9600);
  receive.enableIRIn();

}

void loop() {

  if(receive.decode(&results)){

    value = results.value;
    Serial.println(value, DEC);
    receive.resume();

}

  if (value > 0) {

    digitalWrite(13, HIGH);
    delay(100);
    digitalWrite(13, LOW);
    delay(100);

    value--;
  }

}
