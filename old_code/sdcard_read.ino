// old code, don't know if it works
#include <SPI.h>
#include <SD.h>
#include <string.h>

File myFile;
String fileName;

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.println("please enter file name");

  while (fileName == NULL) {
    fileName = Serial.readString();
    fileName = fileName.substring(0, fileName.length() - 1);
  }

  Serial.print("Initializing SD card...");

  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    while (1);
  }
  Serial.println("initialization done.");

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open(fileName, FILE_READ);

  // if the file opened okay, write to it:
  if (myFile) {
    char temp = myFile.read();
    while(temp != -1) {
      Serial.write(temp);
      temp = myFile.read();
    }
    myFile.close();
    Serial.println();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening " + fileName);
  }

}

void loop() {
  // nothing happens after setup
}
