// old code, don't know if it works

int led = 2;
int button = 13;
int lastState = 0;
int ledState = 0;
int buttonState = 0;
long lastTime = 0;
long now = 0;

void setup() {
  Serial.begin(9600);

  pinMode(button, INPUT_PULLUP);
  pinMode(led, OUTPUT);
}

void loop() {

  buttonState = digitalRead(button);

  if (buttonState != lastState) {

    if (buttonState == HIGH) {

        now = millis() - lastTime;
        if (now < 500) {
         Serial.println(now);
          ledState = HIGH;
          digitalWrite(led, ledState);
        }

    }

    lastTime = millis();
    lastState = buttonState;
    delay(50);

  }

  now = millis() - lastTime;
  if (now > 500 && ledState == HIGH && buttonState == LOW) {
    Serial.println(now);
    ledState = LOW;
    digitalWrite(led, ledState);
  }

}
