#define NUM_OF_LEDS 2
#define PROP_DELAY  20

const uint8_t LEDS[] = {12, 13};
const uint8_t BUTTONS[] = {6, 7};

int state[] = {0, 0};
bool newState[] = {true, true};

void setup() {
    for (int i = 0; i < NUM_OF_LEDS; i++) {
        pinMode(LEDS[i], OUTPUT);
        pinMode(BUTTONS[i], INPUT_PULLUP);
    }
}

void loop() {
    for (int i = 0; i < NUM_OF_LEDS; i++) {
        if (newState[i] && !digitalRead(BUTTONS[i])) {
            delay(PROP_DELAY);
            newState[i] = false;
            state[i]++;
        } else if (digitalRead(BUTTONS[i])) {
            delay(PROP_DELAY);
            newState[i] = true;
        }

        if (state[i] == 0) {
            digitalWrite(LEDS[i], LOW);
        } else if (state[i] == 1) {
            digitalWrite(LEDS[i], HIGH);
        } else {
            state[i] = 0;
        }
    }
}
