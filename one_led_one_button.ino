// Copyright (c) 2020 Abanoub Sameh

const uint8_t LED = 13;
// we use const because the value should never change during runtime
// we use uint8_t to insure that this value is 8bit in size on any machine
const uint8_t BUTTON = 3;
// propagation delay to use, this value can be changed for better results
const uint8_t PROPAGATION_DELAY = 50;
uint8_t state = 0;  // this is the state of the system
// this will tell us if we had already changed the state
bool oldState = true;

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);    // use input pullup
  // that way the button is connected with an internal resistor to 5V
  // and we need to connect the button to GND
}

void loop() {
  // check if we changed the state and if button is being pressed
  if (oldState && !digitalRead(BUTTON)) {
    delay(PROPAGATION_DELAY); // delay to debounce the button
    // i.e. get only one push and remove the mechanincal nonideality of the button
    oldState = false;   // not to read the state again until button is released
    state++;    //  change the state of the system
  } else if (digitalRead(BUTTON)) {
    // if button is released ^
    delay(PROPAGATION_DELAY);
    oldState = true;
    //         ^ allow another reading to be made
  }

  // depending on the state of the system we will change the state of the leds
  if (state == 0) {
    digitalWrite(LED, LOW);
  } else if (state == 1) {
    digitalWrite(LED, HIGH);
  } else {
  // any other state is not allowed, so return to zero
    state = 0;
  }

}
